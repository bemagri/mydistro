#Debian with i3-gaps and polybar
This script install i3-gaps, polybar, vtop, etc., and include all the config files needed for most functionalities.

To open a terminal hit Super+Enter

To open dmenu hit Super+d, then you can choose an application from the list

To lock the screen hit Super+Shift+x

The audio keys should work automatically

The polybar is still being tweaked to work on this setup (e.g., volume is not working)
 

##This script is supposed to be executed in a clean minimal install of Debian.
It was tested on Debian stable 9.4

##How to use it
After a clean minimal install of Debian (without any desktop environment) log in and install git by running
```bash
sudo apt-get install git
```
Then clone this repository to your home folder
```bash
cd ~
git clone https://bitbucket.org/bemagri/mydistro.git
```
Now, navigate to the myDistro folder and run the script
```bash
cd mydistro
./mydistro.sh
```
