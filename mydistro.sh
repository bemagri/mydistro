#!/bin/bash


sudo apt-get update
sudo apt-get install maildir-utils mu4e pavucontrol rofi unzip compton xfce4-terminal pulseaudio neofetch libclang-dev python-virtualenv python3-virtualenv python-jedi python3-jedi zsh network-manager-gnome gnome-keyring iceweasel build-essential git xorg i3 libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev autotools-dev cmake feh lxappearance lightdm ranger fonts-font-awesome x11-utils curl mpd mpc libgnutls28-dev libgtk-3-dev libwebkitgtk-3.0-dev arandr nautilus vlc -y

#installing i3-gaps
cd ~/
git clone https://www.github.com/Airblader/i3 i3-gaps
cd i3-gaps
autoreconf --force --install
rm -rf build/
mkdir -p build && cd build
# Disabling sanitizers is important for release versions!
# The prefix and sysconfdir are, obviously, dependent on the distribution.
../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
make
sudo make install
cd ~/ && sudo rm -rf i3-gaps

#installing polybar
cd ~/
wget -q -O- http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -
echo 'deb http://archive.getdeb.net/ubuntu xenial-getdeb apps' | sudo tee --append /etc/apt/sources.list
sudo apt-get update && sudo apt-get install polybar

#install emacs
sudo apt-get build-dep emacs24 -y
cd ~/
wget http://ftp.gnu.org/gnu/emacs/emacs-25.3.tar.xz
tar -xvf emacs-25.3.tar.xz
cd emacs-25.3/
./autogen.sh && ./configure --with-xwidgets --with-x-toolkit=gtk3
cd src/ && make -j
cd .. && sudo make install
cd ~/ && sudo rm -rf emacs-25.3 && rm emacs-25.3.tar.xz
rm -rf ~/.emacs.d/
mv ~/mydistro/config/emacs.d ~/.emacs.d


#install nodejs and vtop
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get update && sudo apt-get install -y nodejs
sudo npm install -g vtop

#install unimatrix for screensaver/lockscreen
sudo wget https://raw.githubusercontent.com/will8211/unimatrix/master/unimatrix.py -O /usr/local/bin/unimatrix
sudo chmod a+rx /usr/local/bin/unimatrix

#install ubuntu fonts
cd ~ && wget https://assets.ubuntu.com/v1/fad7939b-ubuntu-font-family-0.83.zip
unzip fad7939b-ubuntu-font-family-0.83.zip
cd ubuntu-font-family-0.83
mkdir ~/.fonts && cp *.ttf ~/.fonts
rm -rf ~/ubuntu-font-family-0.83 && rm ~/fad7939b-ubuntu-font-family-0.83.zip

#copying config files
rm -rf ~/.config/
mkdir ~/.config/
mv ~/mydistro/config ~/.config
mv ~/mydistro/xinitrc ~/.xinitrc
mv ~/mydistro/Xdefaults ~/.Xdefaults
mv ~/mydistro/zshrc ~/.zshrc

#install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


